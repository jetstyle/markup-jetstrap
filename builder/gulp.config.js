let builtPath = '../built';
let srcPath = '../src';

const conf = {
	// path config 
	path: {
		base: {
			built: builtPath,
			assets: builtPath + '/assets',
			libs: builtPath + '/assets/libs',
			components: srcPath + '/components'
		},
		img: {
			src: srcPath + '/assets/img/**/*.*',
			components: srcPath + '/components/**/assets/**/*.*',
			built: builtPath + '/assets/img/**/*.*',
			dest: builtPath + '/assets/img'
		},
		fonts: {
			src: srcPath + '/assets/fonts/**/*.*',
			dest: builtPath + '/assets/fonts'
		},
		scripts: {
			src: srcPath + '/assets/scripts/**/*.coffee',
			components: srcPath + '/components/**/*.coffee',
			built: builtPath + '/assets/js/**/*.js',
			dest: builtPath + '/assets/js'
		},
		styles: {
			src: srcPath + '/assets/styles/main.styl',
			all: srcPath + '/assets/styles/**/*.styl',
			components: srcPath + '/components/**/*.styl',
			built: builtPath + '/assets/css/**/*.css',
			dest: builtPath + '/assets/css'
		},
		tpls: {
			base: srcPath + '/templates',
			src: srcPath + '/templates/pages/**/*.jade',	
			layouts: srcPath + '/templates/layouts/*.jade',
			components: srcPath + '/components/**/*.jade',
			dest: builtPath + '/'
		},
		data: {
			src: srcPath + '/templates/pages/**/*.json',
			components: srcPath + '/components/**/*.json'
		}
	},

	// livereload config
	livereload: {
		port: 5000,
		host: '0.0.0.0'
	}
}

exports.conf = conf 