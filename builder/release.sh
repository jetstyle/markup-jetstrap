git checkout master
git pull origin master
gulp build
bower install

date=$(date '+%y-%m-%d')
head_commit_index=$(git log -n 1 --pretty=format:"%h")
zip -r "../markup-$date.zip" ../built

echo 'Enter the commit index, from make diff and press [ENTER]:'
read commit
git diff "$commit" HEAD > "../diff-$head_commit_index..$commit-$date.diff"
