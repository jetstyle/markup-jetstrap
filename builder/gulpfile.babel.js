//packages
import fs from 'fs'
import child_process from 'child_process'
import path from 'path'
import underscore from 'underscore'
import jade from 'jade'
import through from 'through2'
import args from 'yargs';
import pngcrush from 'imagemin-pngcrush'
const del = require('del')

// gulp
import gulp from 'gulp'
import gulpLoadPlugins from 'gulp-load-plugins'
import utils from 'gulp-util'

const g = gulpLoadPlugins()

// config
const conf = require('./gulp.config.js').conf;

// other
const nib = require('nib'),
	  rupture = require('rupture'),
	  jeet = require('jeet');

var consoleErorr = (err) => {
	g.util.beep()
	console.log( err.message )
}

gulp.task( 'img', () => {
	return gulp.src( [ conf.path.img.src,
				conf.path.img.components ] )
		.pipe( g.rename( (path) => {
			path.dirname = ''
			return path
		}) )
		.pipe( gulp.dest( conf.path.img.dest ) );
});

gulp.task( 'fonts', () => {
	return gulp.src( [ conf.path.fonts.src ] )
		.pipe( gulp.dest( conf.path.fonts.dest ) );
});

gulp.task('scripts', () => {
	return gulp.src( [ conf.path.scripts.src, conf.path.scripts.components ] )
		.pipe( g.plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( g.rename( (path) => {
			path.dirname = ''
			return path
		 }) )
		.pipe( g.concat('main.js') )
		.pipe( gulp.dest( conf.path.scripts.dest ) );
});

gulp.task('styles', () => {
	return gulp.src( [ conf.path.styles.src ] )
		.pipe(g.plumber({
			errorHandler: consoleErorr
		}))
		.pipe(g.stylus({
			use: [
				nib(),     // mixin for stylus
				jeet(),    // best future grid system!
				rupture()  // simple media queries in stylus
			]
		}))
		.pipe( gulp.dest( conf.path.styles.dest ) );
});

const parseJson = (string) => {
	return eval("(" + string.toString() + ')')
	// return JSON.parse( string.toString() );
}

// агрегируем блоки из json-файлов
const parseJsonTree = (json) => {
	let blockColection = []

	for (let row in json) {
		if ( json[row].block != undefined ) {
			blockColection.push( json[row].block );
		}
		if (typeof(json[row].data) == 'object') {
			blockColection = blockColection.concat( parseJsonTree(json[row].data) )
		}
	}
	return underscore.unique(blockColection);
}
const componenetsDataCollector = () => {
	let camelCased = (str) => {
		return str.replace(/-([a-z])/g, function (g) {
			return g[1].toUpperCase();
		});
	}

	const files = fs.readdirSync( conf.path.base.components );
	let compoonentsData = {}

	if ( files.length == 0) return {}

	for (let i=0; i < files.length; i++) {
		let dirPath = conf.path.base.components + '/' + files[i].toString();
		let jsonPath = dirPath + '/' + files[i].toString() + '.json';

		if ( fs.statSync( dirPath ).isDirectory() ) {
			try {
				compoonentsData[ camelCased(files[i].toString()) ]
					= parseJson( fs.readFileSync( jsonPath, 'utf8' ) )
			} catch(e) {
				if (e.code != 'ENOENT') {
					e.message = `ComponentDataCollector (parse ${files[i].toString()}): ` + e.message
					consoleErorr(e);
				}
				compoonentsData[ files[i].toString() ] = {}
			}
		}
	}
	return compoonentsData;
}
const pageDataCollector = (file) => {
	const pageDataJson = path.parse(file.path).dir + '/data.json';

	try {
		let stats = fs.statSync( pageDataJson );
		try {
			return parseJson( fs.readFileSync( pageDataJson, 'utf8' ).toString() );
		} catch (eParse) {
			eParse.message = `PageDataCollector (page ./${file.path}): ` + e.message;
			consoleErorr(eParse);
		}
	} catch(e) {
		return {}
	}
}
const jadeHelpers = {
	component: (name, data = {}, modifier = null, attr = null) => {
		try {
			let componentFile = fs.readFileSync(`${conf.path.base.components}/${name}/${name}.jade`, 'utf8' )
			let fn = jade.compile(componentFile + `\n+${name}(data, modifier, attr)`, {pretty: true})
			return fn({data: data, modifier: modifier, attr: attr, helpers: jadeHelpers});
		} catch (e) {
			e.message = `JadeHelpers (component ${name}): ` + e.message
			consoleErorr(e);
			return {}
		}
	}
}
const testCacheExpired = (file, pageData) => {
	var pageText = String(file.contents)

	try {
		var statJsonFile = fs.statSync(path.parse(file.path).dir + '/data.json')
		var statHtmlFile = fs.statSync(`${conf.path.base.built}/${path.basename(file.relative, '.jade')}.html`)
		var statJadeFile = fs.statSync(`${file.path}`)
	} catch (e) {
		// какого-то файла не существует
		return true;
	}

	// проверяем, не поменялись данные или шаблоны страниц
	if (   statJsonFile.mtime > statHtmlFile.mtime
		|| statJadeFile.mtime > statHtmlFile.mtime ) {
		return true;
	}

	// собираем компоненты из шаблона и засовываем их в общую коллекцию компонентов
	let blocks = parseJsonTree(pageData);

	let reList = [
		["(?:!=helpers.component\\([\"\'])(.+)(?:[\"\'])", 'gim'],
		["(?:include .+\/)(.+)", 'gim']
	], m;
	for (let k in reList) {
		let re = new RegExp(reList[k][0], reList[k][1]);

		while ((m = re.exec(pageText)) !== null) {
			if (m.index === re.lastIndex) {
				re.lastIndex++;
			}
			blocks.push( m[1] );
		}
	}
	blocks = underscore.unique(blocks);

	// проверяем, не поменялись данные или шаблоны компонентов
	for (let i in blocks) {
		try {
			let compDataFile = fs.statSync(`${conf.path.base.components}/${blocks[i]}/${blocks[i]}.json`);
			let compTplFile = fs.statSync(`${conf.path.base.components}/${blocks[i]}/${blocks[i]}.jade`);
			if (   compDataFile.mtime > statHtmlFile.mtime
				|| compTplFile.mtime > statHtmlFile.mtime) {
				return true;
			}
		} catch (e) {}
	}

	// проверяем, не поменялись ли лэйауты
	let layouts = fs.readdirSync( conf.path.tpls.base + '/layouts')
	for (let i in layouts) {
		let layoutDataFile = fs.statSync(`${conf.path.tpls.base}/layouts/${layouts[i]}`);

		if ( layoutDataFile.mtime > statHtmlFile.mtime ) {
			return true;
		}
	}
	return false;
}

gulp.task('jade', () => {
	return gulp.src( [ conf.path.tpls.src ] )
		.pipe(g.plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( function(opts) {
			return through.obj( (file, enc, cb) => {
				let pageData = pageDataCollector(file);
				let componenetsData = componenetsDataCollector();

				if ( !args.argv.nc && !testCacheExpired (file, pageData) ) {
					return cb(null)
				}

				let pages = fs.readdirSync( conf.path.tpls.base + '/pages').filter( function(file){
					return file.indexOf('.') == -1
				})

				let data = underscore.extend (
					{ __pages: pages },         // список страниц
					{ helpers: jadeHelpers },   // хэлперы
					{ page: pageData },         // данные страницы
					componenetsData             // данные компонента
				)
				let jadeOpts = {
					basedir: '../src',
					filename: file.path,
					pretty: true
				}
				let contents = String(file.contents);

				const compiled = jade.compile(contents, jadeOpts)(data);

				file.contents = new Buffer(compiled);
				file.path = utils.replaceExtension(file.path, '.html');
				cb(null, file)
			})
		}() )
		.pipe( g.rename( (path) => {
			path.dirname = ''
			return path
		 }) )
		.pipe( g.prettify({indent_size: 2}) )
		.pipe( gulp.dest( conf.path.base.built ) );
});

// add vendor prefixes
gulp.task( 'autoprefixer', () => {
	return gulp.src( conf.path.styles.built )
		.pipe( g.autoprefixer() )
		.pipe( gulp.dest( conf.path.styles.dest ) );
});


// minify js
gulp.task( 'scripts:min', () => {
	return gulp.src( conf.path.scripts.built )
		.pipe( g.plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( g.uglify() )
		.pipe( gulp.dest( conf.path.scripts.dest ) );
});

// minify images
gulp.task( 'images:min', () => {
	return gulp.src( conf.path.img.built )
		.pipe( g.plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( g.imagemin({
			progressive: true,
			svgoPlugins: [
				{
					removeViewBox: false
				}
			],
			use: [
				pngcrush()
			],
		}) )
		.pipe( gulp.dest( conf.path.img.dest ) );
});

// minify css
gulp.task( 'styles:min', () => {
	return gulp.src( conf.path.styles.built )
		.pipe( g.plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( g.base64({
			maxImageSize: 6*1024,
		}) )
		.pipe( g.cleanCSS() )
		.pipe( gulp.dest( conf.path.styles.dest ) );
});


// live reload server
gulp.task('connect', () => {
	g.connect.server({
		root: [ conf.path.base.built, conf.path.base.assets ],
		livereload: true,
		host: conf.livereload.host,
		port: conf.livereload.port
	});
});

// install bower packages
gulp.task('bower', (cb) => {
	return g.bower({
			verbosity: 0
		})
		.pipe(gulp.dest( conf.path.base.libs ))
})


// clear build directory
gulp.task('clear', (cb) => {
	return del([conf.path.base.built], {force: true}, cb);
})

gulp.task('component', () => {
	const componentName = args.argv.title;
	const componentPath = `${conf.path.base.components}/${componentName}`;

	fs.mkdir( componentPath, function( err ){
		if (err) throw err;

		fs.mkdir( `${componentPath}/assets` );
		const paths = [
			`${componentPath}/${componentName}.styl`,
			`${componentPath}/${componentName}.js`,
			`${componentPath}/${componentName}.jade`,
			`${componentPath}/${componentName}.json`,
			`${componentPath}/README.md`
		]

		for (let i in paths) {
			fs.writeFile(
				paths[i], (paths[i].indexOf('json') !== -1 ? '{}' : '')
			);
		}

		console.log( `Компонент ${componentName} успешно создан!` );
	})
})

gulp.task('page', () => {
	const pageName = args.argv.title;
	const pagePath = `${conf.path.tpls.base}/pages/${pageName}`;

	fs.mkdir( pagePath, function( err ){
		if (err) throw err;

		const paths = [
			`${pagePath}/${pageName}.jade`,
			`${pagePath}/data.json`
		]

		for (let i in paths) {
			fs.writeFile(
				paths[i], (paths[i].indexOf('json') !== -1 ? '[]' : '')
			);
		}

		console.log( `Страница ${pageName} успешно создана!` );
	})
})

// watchers
gulp.task( 'watch:all', () => {
	// compile coffee script
	gulp.watch( [ conf.path.scripts.src,
				  conf.path.scripts.components ], gulp.series('scripts') );

	// compile the stylus files
	gulp.watch( [ conf.path.styles.src,
				  conf.path.styles.all,
				  conf.path.styles.components ], gulp.series('styles') );

	// move images
	gulp.watch( [ conf.path.img.src,
				  conf.path.img.components ], gulp.series('img') );

	// move fonts file to ./build directory
	gulp.watch( [ conf.path.fonts.src], gulp.series('fonts') );

	// watch the jade templates and data files
	gulp.watch( [ conf.path.tpls.src,
				  conf.path.tpls.layouts,
				  conf.path.tpls.components,
				  conf.path.data.src,
				  conf.path.data.components ], gulp.series('jade') );
});
gulp.task( 'watch:scripts', () => {
	// compile coffee script
	gulp.watch( [ conf.path.scripts.src,
				  conf.path.scripts.components ], gulp.series('scripts') );
});
gulp.task( 'watch:styles', () => {
	// compile the stylus files
	gulp.watch( [ conf.path.styles.src,
				  conf.path.styles.all,
				  conf.path.styles.components ], gulp.series('styles') );
});
gulp.task( 'watch:img', () => {
	// move images
	gulp.watch( [ conf.path.img.src,
				  conf.path.img.components ], gulp.series('img') );
});
gulp.task( 'watch:fonts', () => {
	// move fonts file to ./build directory
	gulp.watch( [ conf.path.fonts.src], gulp.series('fonts') );
});

gulp.task( 'watch:jade', () => {
	// watch the jade templates and data files
	gulp.watch( [ conf.path.tpls.src,
				  conf.path.tpls.layouts,
				  conf.path.tpls.components,
				  conf.path.data.src,
				  conf.path.data.components ], gulp.series('jade') );
});


gulp.task('watch', gulp.series(
  gulp.parallel('watch:scripts', 'watch:styles', 'watch:img', 'watch:fonts', 'watch:jade')
));

// Run all tasks
gulp.task( 'default', gulp.series('img', 'fonts', 'scripts', 'styles', 'jade') );

// Run all tasks and start watching for changes
gulp.task( 'dev', gulp.parallel('default', 'connect', 'watch'));

// minify all shits
gulp.task( 'minify', gulp.parallel('scripts:min', 'styles:min', 'images:min') );

// Prepare project for production. Run all tasks and minify assets
gulp.task( 'build', gulp.series(
		['clear', 'default', 'autoprefixer'], 'minify', 'bower'
	)
);