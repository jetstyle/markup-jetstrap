Node.prototype.addClass = (className) ->
    if (this.classList)
        this.classList.add(className)
    else
        this.className += ' ' + className

Node.prototype.hasClass = (className) ->
    return (
        if this.classList \
            then this.classList.contains(className) \
            else this.className.indexOf(className) != -1
    )

Node.prototype.removeClass = (className) ->
    if (this.classList)
        this.classList.remove(className)
    else
        this.className.replace(className, '')

Node.prototype.prependChild = (child) ->
    this.insertBefore(child, this.firstChild)